/* The program converts measures of angle
from degrees to radians and vice versa*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	float a = 0;
	char g = 'D';

	printf("Please, enter a value of the angle: D is for degrees, R is for radians\n");
	if (scanf("%f%c", &a, &g) != 2) printf("Wrong input!!!\n");
	else
	{
		if (g != 'D' && g != 'R') printf("Wrong input!!!\n");
		else
		{
			if (g == 'D')
			{
				printf("The angle is ");
				printf("%.2fR\n", a*3.1416 / 180);
				printf("Thank You!\n");
			}
			if (g == 'R')
			{
				printf("The angle is ");
				printf("%.2fD\n", a * 180 / 3.1416);
				printf("Thank You!\n");
			}
		}
	}
	
	
	return 0;
}