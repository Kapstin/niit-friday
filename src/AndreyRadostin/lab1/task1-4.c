/* This programs converts height in metric system to american one and vice versa*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int get_int();// function controling the integer input

int main()
{
	int syst = 0, b = 0, c = 0, b1 = 0;
	float c1 = 0;

	printf("Please, enter Your system: 0 is for American; 1 is for Europeen\n");
	syst = get_int();
	if (syst == 0 || syst == 1)
	{
		if (syst == 0)
		{
			printf("Please, enter Your heght: ft'in\n");
			scanf("%d'%d", &b, &c);
			b1 = (12 * b + c)*2.54 / 100;
			c1 = (12 * b + c)*2.54 - 100 * b1;
			printf("Your heght is: %d m %0.f cm\n",b1,c1);
			
		}
		else
		{
			printf("Please, enter Your heght: m/cm\n");
			scanf("%d/%d", &b, &c);
			b1 = (100 * b + c) / (12 * 2.54);
			c1 = (100 * b + c) / (2.54) - 12 * b1;
			printf("Your heght is: %d ft %0.f in\n",b1,c1);
		}
	}
	else printf("Wrong input\n");
		
	return 0;
}

int get_int()
{
	int input;
	char ch;
	while (scanf("%d", &input) != 1)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" is not integer. \n Please, input ");
		printf(" integer number: ");
	}
		return input;
}